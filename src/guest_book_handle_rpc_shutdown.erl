%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 06. Sep 2021 2:07 PM
%%%-------------------------------------------------------------------
-module(guest_book_handle_rpc_shutdown).
-author("Leonardo R. Teixeira").

%% API

-export([get_paths/0, init/2]).

-import(church, [chain_apply/2]).

-define(DEFAULT_DELAY, 1000).

get_paths() ->
  [ {"/rpc/shutdown", ?MODULE, [ ]} ].

init(Request, State) ->
  chain_apply({Request, State}, [
    fun parse_rpc_payload/1,
    fun extract_arguments/1,
    fun extract_delay/1,
    fun fire_application_shutdown/1,
    fun respond_with_success/1
  ]).

%% -- Internals

parse_rpc_payload({Request, State}) ->
  decode_payload(cowboy_req:read_body(Request), State).

extract_arguments({JsonMap, HttpRequest, State}) ->
  case maps:get(<<"arguments">>, JsonMap, undefined) of
    undefined -> {#{ }, HttpRequest, State};
    Arguments -> {Arguments, HttpRequest, State}
  end.

extract_delay({Arguments, HttpRequest, State}) ->
  {maps:get(<<"delay">>, Arguments, ?DEFAULT_DELAY), HttpRequest, State}.

fire_application_shutdown({Delay, HttpRequest, State}) ->
  fire_delayed_shutdown(Delay),
  {HttpRequest, State}.

respond_with_success({HttpRequest, State}) ->
  {ok, cowboy_req:reply(202, #{ }, <<>>, HttpRequest), State}.

%% -- Support functions

decode_payload({ok, Payload, HttpRequest}, State) ->
  {jsx:decode(Payload), HttpRequest, State}.

fire_delayed_shutdown(Timeout) ->
  spawn(fun() -> timer:sleep(Timeout), init:stop() end).