%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Aug 2021 9:48 AM
%%%-------------------------------------------------------------------
-module(guest_book_sup).
-author("leonardo").

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%% @doc Starts the supervisor
-spec(start_link() -> {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%% @private
%% @doc Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
-spec(init(Args :: term()) ->
  {ok, {SupFlags :: {RestartStrategy :: supervisor:strategy(),
    MaxR :: non_neg_integer(), MaxT :: non_neg_integer()},
    [ChildSpec :: supervisor:child_spec()]}}
  | ignore | {error, Reason :: term()}).
init(_Args) ->
  MaxRestarts = 1000,
  MaxSecondsBetweenRestarts = 3600,
  SupFlags = #{strategy => one_for_one,
    intensity => MaxRestarts,
    period => MaxSecondsBetweenRestarts},

%%  AChild = #{id => 'AName',
%%    start => {'AModule', start_link, []},
%%    restart => permanent,
%%    shutdown => 2000,
%%    type => worker,
%%    modules => ['AModule']},
%%  {ok, {SupFlags, [AChild]}}.

  DatabaseChild = #{
    id => guest_book_database,
    start => { guest_book_database, start_link, [ ] },
    restart => permanent,
    shutdown => 2000,
    type => worker,
    modules => [ guest_book_database ]
  },

  GuestRestMapperChild = #{
    id => guest_rest_mapper,
    start => { guest_rest_mapper, start_link, [ ] },
    restart => permanent,
    shutdown => 2000,
    type => worker,
    modules => [ guest_rest_mapper ]
  },

  GuestUseCasesChild = #{
    id => guest_book_use_cases,
    start => { guest_book_use_cases, start_link, [ ] },
    restart => permanent,
    shutdown => 2000,
    type => worker,
    modules => [ guest_book_use_cases ]
  },

  { ok, { SupFlags, [
    DatabaseChild,
    GuestRestMapperChild,
    GuestUseCasesChild
  ] } }.

%%%===================================================================
%%% Internal functions
%%%===================================================================
