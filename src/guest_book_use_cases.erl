%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(guest_book_use_cases).

-behaviour(gen_server).

-export([start_link/0, create_guest/1, update_guest/1, delete_guest/1,
  find_guest_by_id/1, find_all_guests/0, guest_exists_with_id/1, stop/0]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-import(church, [chain_apply/2]).

-include("guest_book_records.hrl").

-define(SERVER, ?MODULE).

-opaque guest_book_use_cases_state() :: [any()].

-export_type([guest_book_use_cases_state/0]).

-type operation_error() :: { error, Reason :: string() }.
-type create_guest_request() :: { create, G :: #guest{ } }.
-type update_guest_request() :: { update, G :: #guest{ } }.
-type delete_guest_request() :: { delete, Id :: pos_integer() }.
-type find_guest_by_id_request() :: { find_by_id, Id :: pos_integer() }.
-type guest_exists_request() :: { exists_with_id, Id :: pos_integer() }.
-type find_all_guests_request() :: find_all.
-type guest_request() :: create_guest_request()
  | guest_exists_request()
  | update_guest_request()
  | delete_guest_request()
  | find_guest_by_id_request()
  | find_all_guests_request().

%%%===================================================================
%%% Client interface
%%%===================================================================

%% @doc Creates a new guest and returns the new key.
-spec(create_guest(G :: #guest{ }) -> pos_integer() | operation_error()).
create_guest(G = #guest{ }) ->
  gen_server:call(?SERVER, { create, G }).

%% @doc Updates an existing guest by id.
-spec(update_guest(G :: #guest{ }) -> ok | operation_error()).
update_guest(G = #guest{ }) ->
  gen_server:call(?SERVER, { update, G }).

%% @doc Removes a single guest by Id.
-spec(delete_guest(Id :: pos_integer()) -> ok | operation_error()).
delete_guest(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, { delete, Id }).

%% @doc Find a single guest by Id.
-spec(find_guest_by_id(Id :: pos_integer()) -> #guest{ } | operation_error()).
find_guest_by_id(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, { find_by_id, Id }).

%% @doc Tells if a guest exists by Id.
-spec(guest_exists_with_id(Id :: pos_integer()) -> boolean() | operation_error()).
guest_exists_with_id(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, { exists_with_id, Id }).

%% @doc Returns all guests in the database.
-spec(find_all_guests() -> [#guest{ }] | operation_error()).
find_all_guests() ->
  gen_server:call(?SERVER, find_all).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

stop() ->
  gen_server:stop(?SERVER).

init([]) ->
  {ok, [ ]}.

-spec(handle_call(
    Request :: guest_request(),
    From :: {pid(), Tag :: term()},
    State :: guest_book_use_cases_state()) ->
  {reply, Reply :: term(), State :: guest_book_use_cases_state() }).
handle_call({ _, #guest{name = undefined}}, _From, State) ->
  {reply, {error, "Guest name is required"}, State};

handle_call({ _, #guest{email = undefined}}, _From, State) ->
  {reply, {error, "Guest email is required"}, State};

handle_call({ update, #guest{id = undefined}}, _From, State) ->
  {reply, {error, "Guest id is required"}, State};

handle_call({ update, G = #guest{ }}, _From, State) ->
  {reply, handle_update(G), State};

handle_call({ create, G = #guest{ }}, _From, State) ->
  Reply = guest_book_database:insert_guest(stamp_time(G)),
  {reply, Reply, State};

handle_call({ delete, Id }, _From, State) ->
  ok = guest_book_database:delete_guest(Id),
  {reply, ok, State};

handle_call(find_all, _From, State) ->
  {reply, guest_book_database:find_all_guests(), State};

handle_call({ find_by_id, Id }, _From, State) ->
  {reply, guest_book_database:find_guest_by_id(Id), State};

handle_call({ exists_with_id, Id }, _From, State) ->
  {reply, guest_book_database:guest_exists_with_id(Id), State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_update(G = #guest{ }) ->
  chain_apply(G, [
    fun find_existing_guest/1,
    fun copy_fields/1,
    fun update_database/1
  ]).

find_existing_guest(New = #guest{ }) ->
  {guest_book_database:find_guest_by_id(New#guest.id), New}.

copy_fields({not_found, _}) -> {break, ok};
copy_fields({Existing = #guest{ }, New = #guest{ }}) ->
  merge_guest_records(Existing, New).

update_database(Merged = #guest{ }) ->
  guest_book_database:update_guest(Merged).

merge_guest_records(Existing = #guest{ }, New = #guest{ }) ->
  Existing#guest{
    name = New#guest.name,
    email = New#guest.email,
    date = New#guest.date
  }.

stamp_time(G = #guest{ }) ->
  G#guest{ date = erlang:localtime() }.
