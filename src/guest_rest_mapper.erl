%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Aug 2021 8:07 AM
%%%-------------------------------------------------------------------
-module(guest_rest_mapper).
-author("leonardo").

-behaviour(gen_server).

%% API
-export([start_link/0, from_json/1, from_json/2, to_json/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-include("guest_book_records.hrl").

-import(lists, [zip/2, seq/2, foldl/3]).

-type guest_rest_mapper_state() :: [{atom(), integer()}].
-type to_json_input() :: #guest{} | [#guest{}] | [].


%%%===================================================================
%%% API
%%%===================================================================

%% @doc Transforms a json binary back into a guest record.
-spec(from_json(JsonBinary :: binary()) -> #guest{ }).
from_json(JsonBinary) when is_binary(JsonBinary) ->
  gen_server:call(?SERVER, { from_json, JsonBinary }).

%% @doc Transforms a json binary back into a guest record
%% overriding the id field.
-spec(from_json(JsonBinary :: binary(), Id :: pos_integer()) -> #guest{}).
from_json(JsonBinary, Id) when is_binary(JsonBinary) ->
  gen_server:call(?SERVER, { from_json, JsonBinary, Id }).

%% @doc Transforms a guest record into a json binary
-spec(to_json(G :: to_json_input()) -> binary()).
to_json(G = #guest{ }) ->
  gen_server:call(?SERVER, { to_json, G });

to_json(Guests) when is_list(Guests) ->
  gen_server:call(?SERVER, { to_json, Guests }).

%% @doc Spawns the server and registers the local name (unique)
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%% @private
%% @doc Initializes the server
-spec(init(Args :: term()) ->
  {ok, State :: guest_rest_mapper_state()}
  | {ok, State :: guest_rest_mapper_state(), timeout() | hibernate}
  | {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, get_guest_fields()}.

%% @private
%% @doc Handling call messages
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: guest_rest_mapper_state()) ->
  {reply, Reply :: term(), NewState :: guest_rest_mapper_state()}
  | {reply, Reply :: term(), NewState :: guest_rest_mapper_state(), timeout() | hibernate}
  | {noreply, NewState :: guest_rest_mapper_state()}
  | {noreply, NewState :: guest_rest_mapper_state(), timeout() | hibernate}
  | {stop, Reason :: term(), Reply :: term(), NewState :: guest_rest_mapper_state()}
  | {stop, Reason :: term(), NewState :: guest_rest_mapper_state()}).
handle_call({ from_json, JsonBinary }, _From, State) ->
  {reply, to_record(jsx:decode(JsonBinary), State), State};

handle_call({ from_json, JsonBinary, Id }, _From, State) ->
  {reply, to_record(override_id(jsx:decode(JsonBinary), Id), State), State};

handle_call({ to_json, Guest = #guest{ } }, _From, State) ->
  {reply, jsx:encode(to_term(Guest, State)), State};

handle_call({ to_json, Guests }, _From, State) when is_list(Guests) ->
  {reply, jsx:encode([to_term(G, State) || G <- Guests]), State}.

%% @private
%% @doc Handling cast messages
-spec(handle_cast(Request :: term(), State :: guest_rest_mapper_state()) ->
  {noreply, NewState :: guest_rest_mapper_state()}
  | {noreply, NewState :: guest_rest_mapper_state(), timeout() | hibernate}
  | {stop, Reason :: term(), NewState :: guest_rest_mapper_state()}).
handle_cast(_Request, State) ->
  {noreply, State}.

%% @private
%% @doc Handling all non call/cast messages
-spec(handle_info(Info :: timeout() | term(), State :: guest_rest_mapper_state()) ->
  {noreply, NewState :: guest_rest_mapper_state()}
  | {noreply, NewState :: guest_rest_mapper_state(), timeout() | hibernate}
  | {stop, Reason :: term(), NewState :: guest_rest_mapper_state()}).
handle_info(_Info, State) ->
  {noreply, State}.

%% @private
%% @doc This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: guest_rest_mapper_state()) -> term()).
terminate(_Reason, _State) ->
  ok.

%% @private
%% @doc Convert process state when code is changed
-spec(code_change(OldVsn :: term() | {down, term()}, State :: guest_rest_mapper_state(),
    Extra :: term()) ->
  {ok, NewState :: guest_rest_mapper_state()}
  | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

to_term(G = #guest{ }, Fields) ->
  F = fun({Field, Position}, Map) -> append_to_map(Map, G, Field, Position) end,
  foldl(F, #{ }, Fields).

to_record(JsonMap, Fields) ->
  list_to_tuple([guest | get_ordered_values(JsonMap, Fields)]).

get_ordered_values(JsonMap, Fields) ->
  [maps:get(atom_to_binary(K), JsonMap, undefined) || {K, _} <- Fields].

append_to_map(Map, Guest, Field, Position) ->
  case element(Position, Guest) of
    undefined -> Map;
    Value -> maps:put(atom_to_binary(Field), Value, Map)
  end.

get_guest_fields() ->
  zip(record_info(fields, guest), seq(2, record_info(size, guest))).

override_id(JsonMap, Id) -> JsonMap#{<<"id">> => Id}.