%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Aug 2021 9:47 AM
%%%-------------------------------------------------------------------
-module(guest_book_app).
-author("leonardo").

-behaviour(application).

-import(lists, [foldl/3]).
-import(guest_book_env, [get_http_port/0]).

%% Application callbacks
-export([start/2, stop/1]).

%%%===================================================================
%%% Application callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application is started using
%% application:start/[1,2], and should start the processes of the
%% application. If the application is structured according to the OTP
%% design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%
%% @end
%%--------------------------------------------------------------------
-spec(start(StartType :: normal | {takeover, node()} | {failover, node()},
    StartArgs :: term()) ->
  {ok, pid()} |
  {ok, pid(), State :: term()} |
  {error, Reason :: term()}).
start(_StartType, _StartArgs) ->
  Dispatch = cowboy_router:compile([
    {'_', collect_paths([
      guest_book_handle_rpc_shutdown,
      guest_book_handle_rpc_recreate_database,
      guest_book_handle_api_docs,
      guest_book_handle_api_specs,
      guest_book_handle_guests_collection,
      guest_book_handle_single_guest
    ])}
  ]),
  {ok, _} = cowboy:start_clear(http, [{port, get_http_port()}], #{
    env => #{dispatch => Dispatch}
  }),
  lager:info("cowboy is running"),
  guest_book_sup:start_link().

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application has stopped. It
%% is intended to be the opposite of Module:start/2 and should do
%% any necessary cleaning up. The return value is ignored.
%%
%% @end
%%--------------------------------------------------------------------
-spec(stop(State :: term()) -> term()).
stop(_State) ->
  ok = cowboy:stop_listener(http),
  ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

collect_paths(Handlers) ->
  FInner = fun(P, Paths) -> [P | Paths] end,
  FOuter = fun(H, Paths) -> foldl(FInner, Paths, apply(H, get_paths, [ ])) end,
  foldl(FOuter, [ ], Handlers).
