%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 06. Sep 2021 3:16 PM
%%%-------------------------------------------------------------------
-module(guest_book_handle_rpc_recreate_database).
-author("Leonardo R. Teixeira").

%% API

-export([get_paths/0, init/2]).

-import(church, [chain_apply/2]).

-define(DEFAULT_DELAY, 3000).

get_paths() ->
  [ {"/rpc/recreate-database", ?MODULE, [ ]} ].

init(Request, State) ->
  chain_apply({Request, State}, [
    fun fire_database_recreation/1,
    fun respond_with_success/1
  ]).

fire_database_recreation(State) ->
  spawn(fun() -> guest_book_database:recreate() end),
  State.

respond_with_success({HttpRequest, State}) ->
  {ok, cowboy_req:reply(202, #{ }, <<>>, HttpRequest), State}.
