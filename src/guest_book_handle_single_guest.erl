%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Aug 2021 1:17 PM
%%%-------------------------------------------------------------------
-module(guest_book_handle_single_guest).
-author("Leonardo R. Teixeira").

%% API
-export([get_paths/0, init/2, allowed_methods/2, content_types_provided/2,
  content_types_accepted/2, handle_json_request/2, resource_exists/2,
  delete_resource/2, options/2]).

-import(guest_book_use_cases, [update_guest/1, delete_guest/1,
  find_guest_by_id/1, guest_exists_with_id/1]).

-import(church, [chain_apply/2]).

-define(HEADER_ACL_METHODS, <<"access-control-allow-methods">>).
-define(HEADER_ACL_ORIGIN, <<"access-control-allow-origin">>).
-define(HEADER_ACL_HEADERS, <<"access-control-allow-headers">>).
-define(VALUE_ACL_METHODS, <<"GET, PUT, DELETE, OPTIONS">>).
-define(VALUE_ACL_HEADERS, <<"content-type">>).
-define(VALUE_ACL_ORIGIN, <<"*">>).

-include("guest_book_records.hrl").

get_paths() ->
  [ {"/rest/guests/:guest_id", ?MODULE, [ ]} ].

%% -- Cowboy flow

init(Request, State) ->
  {cowboy_rest, Request, State}.

allowed_methods(Request, State) ->
  {[<<"GET">>, <<"PUT">>, <<"DELETE">>, <<"OPTIONS">>], Request, State}.

content_types_provided(Request, State) ->
  {get_provider_list(), Request, State}.

content_types_accepted(Request, State) ->
  {get_accepted_list(), Request, State}.

handle_json_request(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun get_guest_id/1,
    fun consult_database/1,
    fun assemble_response/1,
    fun set_response_headers/1
  ]);

handle_json_request(Request = #{method := <<"PUT">>}, State) ->
  chain_apply({Request, State}, [
    fun get_guest_id/1,
    fun get_request_body/1,
    fun parse_request_body/1,
    fun update_database/1,
    fun assemble_response/1,
    fun set_response_headers/1
  ]).

options(Request, State) ->
  chain_apply({ok, Request, State}, [
    fun set_options_headers/1,
    fun set_response_headers/1
  ]).

delete_resource(Request, State) ->
  chain_apply({Request, State}, [
    fun get_guest_id/1,
    fun run_delete_guest/1,
    fun set_response_headers/1
  ]).

resource_exists(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun get_guest_id/1,
    fun consult_guest_exists/1,
    fun set_response_headers/1
  ]);

resource_exists(Request, State) ->
  set_response_headers({true, Request, State}).

%% -- Internals

get_provider_list() ->
  [{<<"application/json">>, handle_json_request}].

get_accepted_list() ->
  [{<<"application/json">>, handle_json_request}].

get_guest_id({Request, State}) ->
  {binary_to_integer(cowboy_req:binding(guest_id, Request)), {Request, State}}.

get_request_body({Id, {Request, State}}) ->
  {Id, cowboy_req:read_body(Request), State}.

consult_database({Id, State}) ->
  {find_guest_by_id(Id), State}.

run_delete_guest({Id, {Request, State}}) ->
  delete_guest(Id),
  {true, Request, State}.

consult_guest_exists({Id, {Request, State}}) ->
  {guest_exists_with_id(Id), Request, State}.

parse_request_body({Id, {ok, Payload, Request}, State}) ->
  {guest_rest_mapper:from_json(Payload, Id), {Request, State}}.

update_database({Guest, State}) ->
  {update_guest(Guest), State}.

assemble_response({G = #guest{ }, {Request, State}}) ->
  {guest_rest_mapper:to_json(G), Request, State};

assemble_response({{error, Reason}, {Request, State}}) ->
  {false, make_explanation(Request, Reason), State};

assemble_response({ok, {Request, State}}) ->
  {true, Request, State}.

set_response_headers({Payload, Request, State}) ->
  {Payload, set_allow_origin(Request), State}.

set_options_headers({Payload, Request, State}) ->
  {Payload, add_options_headers(Request), State}.

add_options_headers(Request) ->
  chain_apply(Request, [
    fun set_acl_headers_allowed/1,
    fun set_acl_methods_allowed/1
  ]).

set_allow_origin(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_ORIGIN, ?VALUE_ACL_ORIGIN, Request).

set_acl_methods_allowed(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_METHODS, ?VALUE_ACL_METHODS, Request).

set_acl_headers_allowed(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_HEADERS, ?VALUE_ACL_HEADERS, Request).

make_explanation(Request, Reason) ->
  cowboy_req:set_resp_body(jsx:encode(make_message(Reason)), Request).

make_message(Content) ->
  #{message => list_to_binary(Content)}.
