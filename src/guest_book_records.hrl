%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Aug 2021 1:13 PM
%%%-------------------------------------------------------------------
-author("leonardo").

-type date() :: { pos_integer(), 1..8, 1..31 }.
-type time() :: { 0..23, 0..59, 0..59 }.
-type datetime() :: { date(), time() }.

-record(generator, { id, value }).

-record(guest, {
  id :: pos_integer(),
  date :: datetime() | undefined,
  name :: binary(),
  email :: binary()
}).