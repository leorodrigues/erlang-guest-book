%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(guest_book_database).

-behaviour(gen_server).

-export([recreate/0, insert_guest/1, update_guest/1, delete_guest/1,
  find_all_guests/0, find_guest_by_id/1, guest_exists_with_id/1,
  dump_to_text_file/0, load_from_text_file/1]).

-export([start_link/0, init/1, handle_call/3, handle_cast/2, handle_info/2,
  terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-record(gen_state, { }).

-include("guest_book_records.hrl").

recreate() ->
  gen_server:call(?SERVER, recreate).

dump_to_text_file() ->
  gen_server:call(?SERVER, dump_to_text_file).

load_from_text_file(Path) ->
  gen_server:call(?SERVER, {load_from_text_file, Path}).

insert_guest(Guest = #guest{ }) ->
  gen_server:call(?SERVER, { insert_guest, Guest }).

update_guest(Guest = #guest{ }) ->
  gen_server:call(?SERVER, { update_guest, Guest }).

delete_guest(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, { delete_guest, Id }).

find_all_guests() ->
  gen_server:call(?SERVER, find_all_guests).

find_guest_by_id(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, { find_guest_by_id, Id }).

guest_exists_with_id(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, { guest_exists_with_id, Id }).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  application:set_env(mnesia, dir, guest_book_paths:get_mnesia_path()),
  application:start(mnesia),
  gen_server:start_link({local, ?SERVER}, ?MODULE, [ ], [ ]).

init([]) ->
  {ok, #gen_state{}}.

handle_call({ insert_guest, G = #guest{ } }, _From, State = #gen_state{ }) ->
  NextId = mnesia:dirty_update_counter(generator, guest, 1),
  mnesia:transaction(fun() -> mnesia:write(G#guest{ id = NextId }) end),
  {reply, NextId, State};

handle_call({ update_guest, G = #guest{ } }, _From, State = #gen_state{ }) ->
  {reply, handle_update_guest(G), State};

handle_call({ delete_guest, Id }, _From, State = #gen_state{ }) ->
  mnesia:transaction(fun() -> mnesia:delete({ guest, Id }) end),
  {reply, ok, State};

handle_call({ guest_exists_with_id, Id }, _From, State = #gen_state{ }) ->
  {reply, handle_guest_exists_with_id(Id), State};

handle_call({ find_guest_by_id, Id }, _From, State = #gen_state{ }) ->
  T = fun() -> mnesia:read(guest, Id, write) end,
  Reply = case mnesia:transaction(T) of
    {atomic, [Guest]} -> Guest;
    {atomic, [ ]} -> not_found
  end,
  {reply, Reply, State};

handle_call(find_all_guests, _From, State = #gen_state{ }) ->
  T = fun() -> mnesia:select(guest, [{'_', [ ], [ '$_' ]}]) end,
  {atomic, Reply} = mnesia:transaction(T),
  {reply, Reply, State};

handle_call(recreate, _From, State = #gen_state{ }) ->
  ensure_mnesia_path_exists(),
  application:stop(mnesia),
  ok = try_recreating_schema(),
  application:start(mnesia),
  create_tables(),
  mnesia:info(),
  {reply, ok, State};

handle_call(dump_to_text_file, _From, State = #gen_state{ }) ->
  ensure_mnesia_export_path_exists(),
  ok = mnesia:dump_to_textfile(guest_book_paths:get_mnesia_export_file_path()),
  {reply, ok, State};

handle_call({load_from_text_file, Path}, _From, State = #gen_state{ }) ->
  {atomic, ok} = mnesia:load_textfile(Path),
  {reply, ok, State};

handle_call(_Request, _From, State = #gen_state{ }) ->
  {reply, ok, State}.

handle_cast(_Request, State = #gen_state{ }) ->
  {noreply, State}.

handle_info(_Info, State = #gen_state{ }) ->
  {noreply, State}.

terminate(_Reason, _State = #gen_state{ }) ->
  application:stop(mnesia),
  ok.

code_change(_OldVsn, State = #gen_state{ }, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_guest_exists_with_id(Id) ->
  T = fun() -> mnesia:read(guest, Id, write) end,
  case mnesia:transaction(T) of
    {atomic, [ _ ]} -> true;
    {atomic, [ ]} -> false
  end.

handle_update_guest(G = #guest{ }) ->
  case handle_guest_exists_with_id(G#guest.id) of
    true -> mnesia:transaction(fun() -> mnesia:write(G) end), ok;
    false -> ok
  end.

try_recreating_schema() ->
  case mnesia:create_schema([node()]) of
    { error, { _, { already_exists, _ } } } ->
      ok = mnesia:delete_schema([node()]),
      try_recreating_schema();
    AnythingElse ->
      AnythingElse
  end.

create_tables() ->
  { atomic, ok } = mnesia:create_table(generator, [
    { disc_copies, [node()]},
    { attributes, record_info(fields, generator) }
  ]),
  { atomic, ok } = mnesia:create_table(guest, [
    { type, ordered_set },
    { disc_copies, [node()] },
    { attributes, record_info(fields, guest) }
  ]).

ensure_mnesia_path_exists() ->
  filelib:ensure_dir(guest_book_paths:get_mnesia_path()).

ensure_mnesia_export_path_exists() ->
  filelib:ensure_dir(guest_book_paths:get_mnesia_export_file_path()).


