%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Aug 2021 1:12 PM
%%%-------------------------------------------------------------------
-module(guest_book_handle_guests_collection).
-author("leonardo").

%% -- API
-export([get_paths/0, init/2, allowed_methods/2, handle_json_request/2,
  content_types_provided/2, content_types_accepted/2, resource_exists/2, options/2]).

-import(guest_book_use_cases, [find_all_guests/0, create_guest/1]).
-import(church, [chain_apply/2]).

-define(HEADER_ACL_METHODS, <<"access-control-allow-methods">>).
-define(HEADER_ACL_ORIGIN, <<"access-control-allow-origin">>).
-define(HEADER_ACL_HEADERS, <<"access-control-allow-headers">>).
-define(VALUE_ACL_METHODS, <<"GET, POST, OPTIONS">>).
-define(VALUE_ACL_HEADERS, <<"content-type">>).
-define(VALUE_ACL_ORIGIN, <<"*">>).

get_paths() ->
  [ {"/rest/guests", ?MODULE, [ ]} ].

%% -- Cowboy flow

init(Request, State) ->
  {cowboy_rest, Request, State}.

allowed_methods(Request, State) ->
  {[<<"GET">>, <<"POST">>, <<"OPTIONS">>], Request, State}.

content_types_provided(Request, State) ->
  {get_provider_list(), Request, State}.

content_types_accepted(Request, State) ->
  {get_accepted_list(), Request, State}.

resource_exists(Request = #{method := <<"POST">>}, State) ->
  {false, Request, State};

resource_exists(Request, State) ->
  {true, Request, State}.

handle_json_request(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun consult_database/1,
    fun assemble_response/1,
    fun set_response_headers/1
  ]);

handle_json_request(Request = #{method := <<"POST">>}, State) ->
  chain_apply({Request, State}, [
    fun read_request_body/1,
    fun parse_json/1,
    fun invoke_create_guest/1,
    fun assemble_response/1,
    fun set_response_headers/1
  ]).

options(Request, State) ->
  chain_apply({ok, Request, State}, [
    fun set_options_headers/1,
    fun set_response_headers/1
  ]).

%% -- Internals

consult_database({Request, State}) ->
  {guest_rest_mapper:to_json(find_all_guests()), Request, State}.

read_request_body({Request, State}) ->
  {cowboy_req:read_body(Request), State}.

parse_json({{ok, RequestBody, Request}, State}) ->
  {guest_rest_mapper:from_json(RequestBody), {Request, State}}.

invoke_create_guest({NewGuest, State}) ->
  {create_guest(NewGuest), State}.

assemble_response({Id, {Request, State}}) when is_integer(Id) ->
  {{true, make_location_path(Id)}, Request, State};

assemble_response({{error, Reason}, {Request, State}}) ->
  {false, make_explanation(Request, Reason), State};

assemble_response({Payload, Request, State}) ->
  {Payload, Request, State}.

set_response_headers({Payload, Request, State}) ->
  {Payload, set_allow_origin(Request), State}.

set_options_headers({Payload, Request, State}) ->
  {Payload, add_options_headers(Request), State}.

add_options_headers(Request) ->
  chain_apply(Request, [
    fun set_acl_headers_allowed/1,
    fun set_acl_methods_allowed/1
  ]).

set_allow_origin(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_ORIGIN, ?VALUE_ACL_ORIGIN, Request).

set_acl_methods_allowed(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_METHODS, ?VALUE_ACL_METHODS, Request).

set_acl_headers_allowed(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_HEADERS, ?VALUE_ACL_HEADERS, Request).

get_provider_list() ->
  [{<<"application/json">>, handle_json_request}].

get_accepted_list() ->
  [{<<"application/json">>, handle_json_request}].

make_location_path(Id) when is_integer(Id) ->
  io_lib:format("/rest/guests/~p", [Id]).

make_explanation(Request, Reason) ->
  cowboy_req:set_resp_body(jsx:encode(make_message(Reason)), Request).

make_message(Content) ->
  #{message => list_to_binary(Content)}.