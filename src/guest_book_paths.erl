%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Aug 2021 9:02 AM
%%%-------------------------------------------------------------------
-module(guest_book_paths).
-author("leonardo").

-export([get_swagger_file_path/0, get_mnesia_path/0,
  get_mnesia_export_dir_path/0, get_mnesia_export_file_path/0]).

-define(TIME_STAMP_FMT, "~.4.0w-~.2.0w-~.2.0wT~.2.0w-~.2.0w-~.2.0wZ").

get_swagger_file_path() ->
  filename:join([code:priv_dir(guest_book_app), "specs", "swagger.json"]).

get_mnesia_path() ->
  filename:join([os:getenv("HOME"), ".mnesia", node(), "guest_book"]).

get_mnesia_export_dir_path() ->
  filename:join([os:getenv("HOME"), ".mnesia", node(), "guest_book_export"]).

get_mnesia_export_file_path() ->
  filename:join([get_mnesia_export_dir_path(), get_time_stamp()]).

get_time_stamp() ->
  io_lib:format("~s.terms", [time_to_string(erlang:localtime())]).

time_to_string({{Year, Month, Day}, {Hour, Minute, Second}}) ->
  io_lib:format(?TIME_STAMP_FMT, [Year, Month, Day, Hour, Minute, Second]).
