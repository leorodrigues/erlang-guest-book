%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 09. Sep 2021 2:27 PM
%%%-------------------------------------------------------------------
-module(guest_book_use_cases_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").
-include("../src/guest_book_records.hrl").

-export([all/0, init_per_testcase/2, end_per_testcase/2,
  init_per_suite/1, end_per_suite/1]).

-export([create_guest_1/1, create_guest_2/1, create_guest_3/1,
  update_guest_1/1, update_guest_2/1, update_guest_3/1, update_guest_4/1,
  update_guest_5/1, delete_guest/1, find_all/1, find_guest_by_id/1,
  guest_exists_with_id/1]).

-define(NAME_REQUIRED, "Guest name is required").
-define(EMAIL_REQUIRED, "Guest email is required").
-define(ID_REQUIRED, "Guest id is required").
-define(SERVER_NAME, guest_book_use_cases).

%% --- Test cases -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

create_guest_1(_) ->
  ct:comment("Should save the new guest in the database."),
  meck:expect(guest_book_database, insert_guest, fun(_) -> 112358 end),
  112358 = guest_book_use_cases:create_guest(minimal_guest()),
  1 = meck:num_calls(guest_book_database, insert_guest, ['_']).

create_guest_2(_) ->
  ct:comment("Should stop people from creating guests w/o name."),
  {error, ?NAME_REQUIRED} = guest_book_use_cases:create_guest(nameless_guest()),
  0 = meck:num_calls(guest_book_database, insert_guest, ['_']).

create_guest_3(_) ->
  ct:comment("Should stop people from creating guests w/o email."),
  {error, ?EMAIL_REQUIRED} = guest_book_use_cases:create_guest(emailless_guest()),
  0 = meck:num_calls(guest_book_database, insert_guest, ['_']).

update_guest_1(_) ->
  ct:comment("Should successfuly update a guest that does not exist."),
  meck:expect(guest_book_database, find_guest_by_id, fun(_) -> not_found end),
  ok = guest_book_use_cases:update_guest(minimal_guest_for_update()),
  0 = meck:num_calls(guest_book_database, update_guest, ['_']).

update_guest_2(_) ->
  ct:comment("Should successfuly update a guest that exists."),
  meck:expect(guest_book_database, find_guest_by_id, fun(_) -> #guest{ } end),
  meck:expect(guest_book_database, update_guest, fun(_) -> ok end),
  ok = guest_book_use_cases:update_guest(minimal_guest_for_update()),
  1 = meck:num_calls(guest_book_database, update_guest, ['_']).

update_guest_3(_) ->
  ct:comment("Should stop people from updating guests w/o name."),
  {error, ?NAME_REQUIRED} = guest_book_use_cases:update_guest(nameless_guest()),
  0 = meck:num_calls(guest_book_database, find_guest_by_id, ['_']),
  0 = meck:num_calls(guest_book_database, update_guest, ['_']).

update_guest_4(_) ->
  ct:comment("Should stop people from updating guests w/o email."),
  {error, ?EMAIL_REQUIRED} = guest_book_use_cases:update_guest(emailless_guest()),
  0 = meck:num_calls(guest_book_database, find_guest_by_id, ['_']),
  0 = meck:num_calls(guest_book_database, update_guest, ['_']).

update_guest_5(_) ->
  ct:comment("Should stop people from updating guests w/o id."),
  {error, ?ID_REQUIRED} = guest_book_use_cases:update_guest(minimal_guest()),
  0 = meck:num_calls(guest_book_database, find_guest_by_id, ['_']),
  0 = meck:num_calls(guest_book_database, update_guest, ['_']).

delete_guest(_) ->
  ct:comment("Should just delegate 'delete' to the database process."),
  meck:expect(guest_book_database, delete_guest, fun(_) -> ok end),
  ok = guest_book_use_cases:delete_guest(112358),
  1 = meck:num_calls(guest_book_database, delete_guest, [112358]).

find_all(_) ->
  ct:comment("Should load all guests from the database."),
  meck:expect(guest_book_database, find_all_guests, fun() -> [ ] end),
  [ ] = guest_book_use_cases:find_all_guests(),
  1 = meck:num_calls(guest_book_database, find_all_guests, [ ]).

find_guest_by_id(_) ->
  ct:comment("Should simply load a guest from the database."),
  meck:expect(guest_book_database, find_guest_by_id, fun(_Id) -> #guest{ } end),
  #guest{ } = guest_book_use_cases:find_guest_by_id(10),
  1 = meck:num_calls(guest_book_database, find_guest_by_id, [10]).

guest_exists_with_id(_) ->
  ct:comment("Should just delegate 'exists' to the database process."),
  meck:expect(guest_book_database, guest_exists_with_id, fun(_Id) -> true end),
  true = guest_book_use_cases:guest_exists_with_id(10),
  1 = meck:num_calls(guest_book_database, guest_exists_with_id, [10]).

%% --- Support functions --- --- --- --- --- --- --- --- --- --- --- --- --- ---

minimal_guest() ->
  #guest{ name = "Jack", email = "sparrow@domain.com" }.

minimal_guest_for_update() ->
  #guest{ name = "Jack", email = "sparrow@domain.com", id = 42 }.

emailless_guest() -> #guest{ name = "John" }.

nameless_guest() -> #guest{ }.

%% --- Common test API functions --- --- --- --- --- --- --- --- --- --- --- ---

all() -> [create_guest_1, create_guest_2, create_guest_3,
  update_guest_1, update_guest_2, update_guest_3, update_guest_4,
  update_guest_5, delete_guest, find_all, find_guest_by_id,
  guest_exists_with_id].

init_per_testcase(_Case, Config) ->
  meck:new(guest_book_database, [stub_all]),
  Config.

end_per_testcase(_Case, Config) ->
  meck:unload(guest_book_database),
  Config.

init_per_suite(Config) ->
  gen_server:start({local, ?SERVER_NAME}, ?SERVER_NAME, [ ], [ ]),
  Config.

end_per_suite(_Config) ->
  gen_server:stop(?SERVER_NAME, normal, 1000),
  ok.
