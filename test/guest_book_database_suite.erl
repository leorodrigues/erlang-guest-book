%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 12. Sep 2021 11:39 AM
%%%-------------------------------------------------------------------
-module(guest_book_database_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").

-include("../src/guest_book_records.hrl").

-export([all/0, init_per_testcase/2, end_per_testcase/2, init_per_suite/1,
  end_per_suite/1, find_guest_by_id_1/1, find_guest_by_id_2/1,
  guest_exists_with_id_1/1, guest_exists_with_id_2/1, find_all_guests_1/1,
  update_guest_1/1, update_guest_2/1, delete_guest_1/1, delete_guest_2/1]).

-export([insert_1/1, insert_2/1]).

-import(test_support, [load_suite_data/1, load_case_data/3]).

-define(SERVER_NAME, guest_book_database).

delete_guest_1(Config) ->
  ct:comment("Should do nothing with the database if the record is not found"),
  GuestList = load_case_data(Config, delete_guest_1, [guest_list]),

  GuestList = guest_book_database:find_all_guests(),
  ok = guest_book_database:delete_guest(99),
  GuestList = guest_book_database:find_all_guests().

delete_guest_2(Config) ->
  ct:comment("Should remove Jack from the database"),
  AllGuests = load_case_data(Config, delete_guest_2, [all_guests]),
  AllGuests = guest_book_database:find_all_guests(),

  ok = guest_book_database:delete_guest(1),

  NotAllGuests = load_case_data(Config, delete_guest_2, [not_all_guests]),
  NotAllGuests = guest_book_database:find_all_guests().

update_guest_1(_) ->
  ct:comment("Should update the email of Professor Asimov"),
  BeforeUpdate = instantiate_current_asimov(),
  BeforeUpdate = guest_book_database:find_guest_by_id(2),

  ok = guest_book_database:update_guest(BeforeUpdate#guest{
    email = <<"isaac.asimov@mit.edu">>
  }),

  AfterUpdate = instantiate_updated_asimov(),
  AfterUpdate = guest_book_database:find_guest_by_id(2).

update_guest_2(Config) ->
  ct:comment("Should not alter the database if guest doesn't exist"),
  ExpectedList = load_case_data(Config, update_guest_2, [guest_list]),

  ExpectedList = guest_book_database:find_all_guests(),
  ok = guest_book_database:update_guest(#guest{
    id = 99,
    name = <<"John">>,
    email = <<"constantine@dc-commics.com">>
  }),
  ExpectedList = guest_book_database:find_all_guests().

find_all_guests_1(Config) ->
  ct:comment("Should retrieve all guests from the database"),
  ExpectedList = load_case_data(Config, find_all_guests_1, [guest_list]),
  ExpectedList = guest_book_database:find_all_guests().

guest_exists_with_id_1(_) ->
  ct:comment("There should be no guest with id 112358"),
  false = guest_book_database:guest_exists_with_id(112358).

guest_exists_with_id_2(_) ->
  ct:comment("Should find guest number 1 in the database"),
  true = guest_book_database:guest_exists_with_id(1).

find_guest_by_id_1(_) ->
  ct:comment("Should not find the given id in the database"),
  not_found = guest_book_database:find_guest_by_id(112358).

find_guest_by_id_2(_) ->
  ct:comment("Should find 'Lady Lovelace' in the database"),
  LadyLovelace = instantiate_lovelace_record(),
  LadyLovelace = guest_book_database:find_guest_by_id(3).

insert_1(_) ->
  ct:comment("Should insert a new guest and return the ID"),
  4 = guest_book_database:insert_guest(#guest{
    name = "Isaac", email = "aasimov@domain.com"
  }).

insert_2(_) ->
  ct:comment("Should increment the ID with each insert"),
  4 = guest_book_database:insert_guest(#guest{
    name = "Bud", email = "spencer@domain.com"
  }),
  5 = guest_book_database:insert_guest(#guest{
    name = "Terence", email = "hill@domain.com"
  }).

%% --- Support functions --- --- --- --- --- --- --- --- --- --- --- --- --- ---

instantiate_lovelace_record() ->
  {guest,3,{{2021,9,13},{20,28,23}},
    <<"Ada Lovelace">>,<<"lovelace@domain.com">>}.

instantiate_current_asimov() ->
  {guest,2,{{2021,9,13},{20,27,54}},
    <<"Isaac">>,<<"asimov@domain.com">>}.

instantiate_updated_asimov() ->
  {guest,2,{{2021,9,13},{20,27,54}},
    <<"Isaac">>,<<"isaac.asimov@mit.edu">>}.

%% --- Common test API functions --- --- --- --- --- --- --- --- --- --- --- ---

all() -> [find_all_guests_1, find_guest_by_id_1, find_guest_by_id_2,
  guest_exists_with_id_1, guest_exists_with_id_2, insert_1, insert_2,
  update_guest_1, update_guest_2, delete_guest_1, delete_guest_2].

init_per_testcase(_Case, Config) ->
  guest_book_database:recreate(),
  guest_book_database:load_from_text_file(get_dump_file_path(Config)),
  Config.

end_per_testcase(_Case, Config) ->
  Config.

init_per_suite(Config) ->
  gen_server:start({local, ?SERVER_NAME}, ?SERVER_NAME, [ ], [ ]),
  load_suite_data(Config).

end_per_suite(_Config) ->
  gen_server:stop(?SERVER_NAME, normal, 1000),
  ok.

get_dump_file_path(Config) ->
  filename:join(?config(data_dir, Config), "database_dump.terms").
