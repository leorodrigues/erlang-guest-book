%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. Aug 2021 8:38 AM
%%%-------------------------------------------------------------------
-module(test_support).
-include_lib("common_test/include/ct.hrl").

-author("leonardo").

%% API
-export([load_suite_data/1, get_data/2, load_case_data/3]).

load_suite_data(Config) ->
  [{samples, load_terms(get_case_data_path(Config))} | Config].

load_case_data(Config, Case, Path) ->
  test_support:get_data(?config(samples, Config), [Case|Path]).

get_data(Config, [ ]) -> Config;
get_data(Config, [Head|Tail]) ->
  case proplists:get_value(Head, Config, undefined) of
    undefined -> erlang:error("Path element not found", [Head]);
    AnythingElse -> get_data(AnythingElse, Tail)
  end.

get_case_data_path(Config) ->
  filename:join(?config(data_dir, Config), "case_data.terms").

load_terms(Path) ->
  {ok, Terms} = file:consult(Path), Terms.