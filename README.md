# GUEST BOOK - Sample Service

This is a sample service for a guest book, implemented in Erlang. The guest book
theme is simply a justification for implementing some "modern" service features
using Erlang.

The service exposes a rest interface for CRUD operations on a "guest resource"
and an RPC (not Json-RPC 2.0) interface for service maintenance purposes.

## The essential

### Requirements

This service was implemented on top of Erlang/OTP-24. It uses Rebar3 as the
build/deployment tool. Make sure you have both installed and running
on your system.

Some dependencies such as "rebar run" plugin will need you to install C/C++
development tools.

### Get the service

Just clone the repository to your machine as displayed in the example below.

```shell
$ git clone https://bitbucket.org/leorodrigues/erlang-guest-book.git guest_book
$ cd guest_book
```

This will leave you at the HEAD of the master repository which could include
some breaking code. In order to make sure you are running a stable release,
use the highest available tag. At the time of writing of this document, it was
tag `2.0.2`.

_This will show you the latest history. Follow it and pick the highest tag_
```shell
$ git log --oneline
```

_This will check out the tag_
```shell
$ git checkout 2.0.2
```

### Running

Once in the cloned directory, issue the command `rebar shell`
for local execution.

```shell
$ rebar3 shell
```

Rebar3 will begin downloading dependencies and compiling everything that is
necessary as the log lines on the console will show you. Once the logging on
the console stops,  access the service via it's Swagger UI at
`http://localhost:8080/docs/ui`.

### Testing

Testing was done using "Erlang Common Test" framework and "Meck" for mocking
modules. Run `rebar3 ct` as exemplified below to run the tests.

```shell
$ rebar3 ct
```

Once the testing is finished you may access the test results and coverage
analysis in the following path: `<build_dir>/test/index.html`.

## A brief word on design choices

For starters, I'm a beginner in Erlang, so if you are a veteran Erlang developer
it might be the case that you will find some of this source code to contain
unusual or unorthodox implementations, which could cause you to feel some mild
agitation or discomfort. In this light, please stay calm. I'm also
an OOP programmer used to apply Domain Driven Design and strive for separation
of concerns, interface segregation, encapsulation and so on. As a consequence,
my design choices inevitably follow some of those guidelines specially when
applied to the naming of modules, functions and variables, despite my efforts to
divert from the object orientation paradigm.